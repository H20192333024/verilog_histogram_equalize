`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/01/16 10:03:56
// Design Name: 
// Module Name: reflect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 建立映射关系的模块，即映射从原图像素到输出像素的映射关系
//////////////////////////////////////////////////////////////////////////////////


module reflect(
    input clk,
    input rst_n,
    input [7:0] gray,//输入像素灰度级
    input [31:0] count,//输入像素的数量
    input pin_clken,//输入像素时钟

    output reg [7:0] gray_reflect,//输出的映射灰度级
    output reg pout_clken,//输出像素时钟
    output reg bussy//模块忙
    );
    //灰度概率密度函数p(gray)=num(gray)/total_pixel_num 其分布函数为s(i)=sum(p(gray)) k=0...i
    //因为上面的计算中涉及大量的浮点计算，为了避免浮点计算，特意采用下面的方法进行改进
    //改进后的概率密度函数p'(gray)=num(gray) 分布函数s'(i)=sum(p'(gray))) k=0...i
    //原来的映射关系gray_r=s(i)*255(gray->gray_r)
    //新的映射关系gray_r'*gray_level<=s'(i)*255<(gray_r+1)'*(gray_level)(gray->gray_r')
    //gray_level=total_pixel_num/256(总的灰度级)
    //使用数量代替概率密度避免浮点计算
    reg[31:0] cdf;//记录灰度分布函数
    reg [31:0] cdf_r=20'd0;//
    parameter gray_level = 11'd1024;
    parameter gray_level_half = 10'd512;

    reg [7:0] gray_reflect_r;

    reg [1:0] i;//状态机
    always @(posedge clk or negedge rst_n) begin
        if(!rst_n) begin
            cdf<= 20'd0;
            gray_reflect<=8'd0;
            gray_reflect_r<=8'd0;
            i<=3'd0;
            pout_clken<=1'b0;
            bussy<=1'b0;
        end
        else begin
            case(i)
                //第一步接收输入，累加分布函数
                2'd0:begin
                    if(pin_clken)begin
                        //此处是为了加速一下那些不存在的像素，他们的映射将会是0
                        if(count!=32'd0) begin
                            cdf<=cdf+count;
                            bussy<=1'b1;
                            i<=2'd1;
                        end
                        else 
                            pout_clken<=1'b1;
                    end
                    pout_clken<=1'b0;
                end
                //建立映射关系进行输出
                2'd1:begin
                    if(cdf>=cdf_r&&cdf<cdf_r+gray_level_half)begin
                        gray_reflect<=gray_reflect_r;
                        pout_clken<=1'b1;
                        i<=1'b0;
                        bussy<=1'b0;
                    end
                    else if(cdf>=cdf_r+gray_level_half&&cdf<=cdf_r+gray_level) begin
                        if(gray_reflect_r<255) begin
                            gray_reflect<=gray_reflect_r+1'b1;
                        end
                        pout_clken<=1'b1;
                        i<=1'b0;
                        bussy<=1'b0;
                    end
                    else begin
                        if(gray_reflect_r<255) begin
                            gray_reflect_r<=gray_reflect_r+1'b1;
                        end
                        cdf_r<=cdf_r+gray_level;
                    end
                end
            endcase
        end
    end
endmodule

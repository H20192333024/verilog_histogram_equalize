module top_histogram(
    input clk,
    input rst_n,

    input [7:0] gray,
    input pin_clken,

    output reg wen,//高电平表示此模块正忙，无法接受数据
    output wire [7:0] hist_gray,
    output reg pout_clken
);



parameter image_wdith = 10'd512;
parameter image_height = 10'd512;
parameter total_pixel = 20'h40000;

wire wen_hist;//灰度直方图统计模块的忙标志
reg wen_hist_r;

reg [7:0] addr;
wire [7:0] addr_w;
wire [31:0] dout;
wire [31:0] din;
wire wea;//灰度直方图的写入使能

reg [17:0] addr_image;//image RAM的地址
wire [7:0] dout_image;//输出的图像灰度
(* DONT_TOUCH = "1" *) 
reg [7:0] din_image;//输入的图像灰度
reg wea_image;//image RAM的写入使能

reg [7:0] addr_trans;//转换表的地址
wire [7:0] addr_trans_w;//转换表的地址
wire [7:0] dout_trans;//转换表的输出数据
wire [7:0] din_trans;//转换表的输入数据
wire wea_trans;//转换表的写入使能

reg [31:0] pixel_index;//记录直方图已经统计的像素数量

reg [2:0] state;//直方图均衡的状态机，0：统计灰度值 1：采用灰度数量分布代替概率分布进行映射转换

reg [1:0] state_reflct;//建立映射关系时使用的状态机
reg pin_clken_reflect;//映射关系使用的像素时钟

reg pin_clken_hist;
wire pout_clken_reflect;
wire bussy_reflect;//映射模块忙标志
reg [7:0] gray_in_reflect;//映射模块的输入灰度值

reg state_hiostogram;//直方图t统计的状态机

reg [2:0] state_grayout;//完成灰度变换的状态机

always @(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        wea_image<=1'b0;
        pixel_index<=32'd0;
        state<=3'b001;
        state_reflct<=2'd0;
        pin_clken_reflect<=1'b0;
        state_grayout<=3'd0;
        state_hiostogram<=1'd0;
        pin_clken_hist<=1'b0;
        wen<=1'b0;
        pout_clken<=1'b0;
        addr_image<=18'd0;
        din_image<=8'd0;
    end
    else begin
        case(state)
            //统计灰度值和将灰度图像存入RAM
            3'b001:begin
                wen<=(wen==1'b1)?(~wea):wen;
                case(state_hiostogram) 
                    1'd0:begin
                        pin_clken_hist<=1'b0;
                        wea_image<=1'b0;//禁止写入图像存储RAM
                        if(pixel_index<total_pixel) begin
                            if(pin_clken) begin
                                wen<=1'b1;//此时模块的忙于灰度直方图统计模块一致
                                pixel_index<=pixel_index+1'b1;
                                din_image<=gray;//准备将灰度值写入存储图像的RAM
                                addr_image<=pixel_index[17:0];//图像存储RAM的写入地址
                                addr<=gray;//将灰度值作为灰度直方图RAM的地址
                                //wea<=1'b0;//读出使能，读出对应灰度值的当前统计数量
                                state_hiostogram<=1'd1;
                            end
                        end
                        else begin
                            pixel_index<=32'd0;
                            addr<=8'd0;
                            state<=(state<<1);
                        end
                    end
                    1'd1:begin
                        if(!wen_hist)begin
                            pin_clken_hist<=1'b1;//让灰度直方图计算模块读入灰度值和已经统计的数量
                            wea_image<=1'b1;//写入图像存储RAM
                            state_hiostogram<=1'd0;
                        end
                        
                    end
                endcase
            end
            //建立映射关系
            3'b010:begin
                wen<=1'b1;//此时电路一直忙直至完成整个流程
                case (state_reflct)
                    //根据已经赋值好的addr_1读出灰度分布RAM中的数值，并给出像素时钟给reflect模块
                    2'b00:begin
                        if(!bussy_reflect) begin
                            addr_trans<=addr;

                            pin_clken_reflect<=1'b1;
                            gray_in_reflect<=addr;

                            state_reflct<=2'b01;
                        end
                    end
                    //地址+1，准备读出下一个灰度值对应的像素数量
                    2'b01:begin
                        if(addr<255) begin
                            addr<=addr+1'b1;
                        end
                        else begin
                            addr<=8'd0;
                            state<=(state<<1);
                        end
                        pin_clken_reflect<=1'b0;
                        state_reflct<=2'b00;
                    end
                endcase
            end
            //映射
            3'b100:begin
                case(state_grayout)
                    2'b00:begin
                        pout_clken<=1'b0;
                        //wea_image<=1'b0;//前面已经置0了
                        addr_image<=pixel_index;
                        state_grayout<=2'b01;
                    end
                    2'b01:begin
                        addr_trans<=dout_image;
                        state_grayout<=2'b10;
                    end
                    2'b10:begin
                        pout_clken<=1'b1;
                        state_grayout<=2'b00;
                        if(pixel_index<total_pixel) begin
                            pixel_index<=pixel_index+1'b1;
                        end
                    end
                endcase    
            end
        endcase
    end
end

wire rst_hist=rst_n&state[0];//此处可以使state发生变化后一直复位此模块，输出wen_hist=0,wea=0,addr=0,din=0
histogram u_histogram (
    .clk(clk),
    .rst_n(rst_hist),

    .gray(gray),//灰度图片输入
    .pin_clken(pin_clken_hist),//对应的像素时钟
    .pixel_count(dout),//读入RAM中对应灰度值的数量

    .wen(wen_hist),//高电平表示此模块正忙，无法接受数据
    .wea(wea),//写入RAM使能
    .addr(),//写入地址
    .pixel_count_r(din)//写入值
);

//定义一个位宽为32bit，位深为256的单口RAM，存储灰度直方图
blk_mem_gen_0 Ram32x256 (
    .clka(clk),    // input wire clka
    .wea(wea),      // input wire [0 : 0] wea，高电平时为写使能
    .addra(addr),  // input wire [7 : 0] addr
    .dina(din),    // input wire [31 : 0] din
    .douta(dout)  // output wire [31 : 0] dout
);

//图像存储的RAM 512x512
image_mem u_image_mem (
    .clka(clk),    // input wire clk
    .wea(wea_image),      // input wire [0 : 0] wea
    .addra(addr_image),  // input wire [17 : 0] addra
    .dina(din_image),    // input wire [7 : 0] dina
    .douta(dout_image)  // output wire [7 : 0] douta
);

wire rst_reflect=rst_n&state[1];
reflect u_reflect(
    .clk(clk),
    .rst_n(rst_reflect),
    //.gray(gray_in_reflect),
    .gray(addr),
    .count(dout),
    .pin_clken(pin_clken_reflect),

    .gray_reflect(din_trans),
    .pout_clken(wea_trans),
    .bussy(bussy_reflect)
);

//存储灰度变换关系的RAM
transform256x8 u_transform256x8 (
         .clka(clk),    // input wire clka
         .wea(wea_trans),      // input wire [0 : 0] wea
         .addra(addr_trans),  // input wire [7 : 0] addra
         .dina(din_trans),    // input wire [7 : 0] dina
         .douta(hist_gray)  // output wire [7 : 0] douta
);

endmodule
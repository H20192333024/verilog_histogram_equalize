`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2024/01/15 11:17:10
// Design Name:
// Module Name: histogram
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
//此模块为了统计灰度图像的直方图，输入为灰度图像，输出直方图到RAM中进行存储

module histogram (
    input clk,
    input rst_n,

    input [7:0] gray,
    input pin_clken,
    input [31:0] pixel_count,//从外部输入，记录直方图已经统计的像素数量

    output reg wen,//高电平表示此模块正忙，无法接受数据
    output reg wea,//对外部的RAM读写使能，0为读，1为写
    output reg [7:0] addr,//根据输入的灰度值输出地址
    output reg [31:0] pixel_count_r//在已经统计的像素数量的基础上，加上1输出
);
  reg [1:0] state;
  always @(posedge clk or negedge rst_n)
  begin
    if(!rst_n)
    begin
      state<=2'd0;
      wen<=1'b0;
      wea<=1'b0;
      addr<=8'd0;
      pixel_count_r<=32'd0;
    end
    else begin
      case(state)
        2'd0: begin
          wea<=1'b0;//读取直方图RAM的数据
          if(pin_clken) begin
            state<=2'd1;
            addr<=gray;//准备直方图RAM写入的地址
            wen<=1'b1;//处理当前输入的灰度，不接收其它数据
          end
        end
        // 2'd1: begin
        //   state<=2'd2;
        //   pixel_count_r<=pixel_count+1'b1;//直方图已经统计的像素数量加1
        // end
        // 2'd2:begin
        //   state<=2'd0;
        //   wea<=1'b1;//写入数据
        //   wen<=1'b0;//通知外部模块可以在下一个时钟周期输入
        // end
        2'd1: begin
          state<=2'd0;
          pixel_count_r<=pixel_count+1'b1;//直方图已经统计的像素数量加1
          wea<=1'b1;//写入数据
          wen<=1'b0;//通知外部模块可以在下一个时钟周期输入
        end
      endcase
    end
  end
endmodule